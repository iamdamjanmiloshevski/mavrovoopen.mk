<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="LEORON">
    <title>Mavrovo Open 2016</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<header id="header" role="banner">
    <div class="main-nav">
        <div class="container">

            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="index.html">-->
                    <!--&lt;!&ndash;<img class="img-responsive" src="images/logo.png"  alt="logo">&ndash;&gt;-->
                    <!--</a>                    -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="scroll active"><a href="#home">Home</a>
                            <!--<li class="scroll"><a href="#explore">When</a></li>-->
                        <li class="scroll"><a href="#event">Event</a></li>
                        <li class="scroll"><a href="#about">About</a></li>
                        <li class="no-scroll"><a href="#twitter">Board members</a></li>
                        <li class="no-scroll"><a href="#gallery">Gallery</a></li>
                        <li class="scroll"><a href="#contact-sect">Contact</a></li>
                    </ul>
                </div>
                <br/><br/>
            </div>
        </div>
    </div>
</header>
<!--/#header-->

<section id="home">
    <div id="main-slider" class="carousel slide" data-ride="carousel">
        <!--<ol class="carousel-indicators">-->
        <!--<li data-target="#main-slider" data-slide-to="0" class="active"></li>-->
        <!--<li data-target="#main-slider" data-slide-to="1"></li>-->
        <!--<li data-target="#main-slider" data-slide-to="2"></li>-->
        <!--</ol>-->
        <div class="carousel-inner">
            <div class="item active">
                <img class="img-responsive" src="images/slider/MavrovoOpen-MAIN-new2.jpg" alt="slider">
                <!--<div class="carousel-caption">-->
                <!--<h2>register for our next event </h2>-->
                <!--<h4>full event package only @$199</h4>-->
                <!--<a href="#contact">GRAB YOUR TICKETS <i class="fa fa-angle-right"></i></a>-->
                <!--</div>-->
            </div>
            <!--<div class="item">-->
            <!--<img class="img-responsive" src="images/slider/bg2.jpg" alt="slider">	-->
            <!--<div class="carousel-caption">-->
            <!--<h2>register for our next event </h2>-->
            <!--<h4>full event package only @$199</h4>-->
            <!--<a href="#contact">GRAB YOUR TICKETS <i class="fa fa-angle-right"></i></a>-->
            <!--</div>-->
            <!--</div>-->
            <!--<div class="item">-->
            <!--<img class="img-responsive" src="images/slider/bg3.jpg" alt="slider">	-->
            <!--<div class="carousel-caption">-->
            <!--<h2>register for our next event </h2>-->
            <!--<h4>full event package only @$199</h4>-->
            <!--<a href="#contact" >GRAB YOUR TICKETS <i class="fa fa-angle-right"></i></a>-->
            <!--</div>-->
            <!--</div>				-->
        </div>
    </div>
</section>
<!--/#home-->

<section id="explore">
    <div class="container">
        <div class="row">
            <div class="watch">
                <img class="img-responsive" src="images/watch.png" alt="">
            </div>
            <div class="col-md-4 col-md-offset-2 col-sm-5" style="color: #8dc63f;">
                <h2>Coming in...</h2>
            </div>
            <div class="col-sm-7 col-md-6">
                <ul id="countdown">
                    <li>
                        <span class="days time-font">00</span>
                        <p>days </p>
                    </li>
                    <li>
                        <span class="hours time-font">00</span>
                        <p class="">hours </p>
                    </li>
                    <li>
                        <span class="minutes time-font">00</span>
                        <p class="">minutes</p>
                    </li>
                    <li>
                        <span class="seconds time-font">00</span>
                        <p class="">seconds</p>
                    </li>
                </ul>
            </div>
        </div>
        <!--<div class="cart">-->
        <!--<a href="#"><i class="fa fa-shopping-cart"></i> <span>Purchase Tickets</span></a>-->
        <!--</div>-->
    </div>
</section><!--/#explore-->

<section id="event">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div id="event-carousel" class="carousel slide" data-interval="false">
                    <h2 class="heading">You'll be entertained by...</h2>
                    <a class="even-control-left" href="#event-carousel" data-slide="prev"><i
                            class="fa fa-angle-left"></i></a>
                    <a class="even-control-right" href="#event-carousel" data-slide="next"><i
                            class="fa fa-angle-right"></i></a>
                    <div class="carousel-inner text-center">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive"
                                             src="images/performers/AFTERNOON IN AMSTERDAM.jpg" alt="event-image">
                                        <h4 style="font-size: 18px;">Afternoon in Amsterdam</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 17.00-18.00</h6>-->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/FULLSTOP.jpg"
                                             alt="event-image">
                                        <h4>Fullstop</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 18.00-19.00</h6>-->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/DANI.jpg"
                                             alt="event-image">
                                        <h4>Dani Dimitrovska</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 19.00-21.00</h6>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/INTELECT-MUSIC.jpg"
                                             alt="event-image">
                                        <h4>Intellect</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 00.00-01.00</h6>-->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/dj-KNOCKOUT.jpg"
                                             alt="event-image">
                                        <h4>Knockout</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 22.00-23.00</h6>-->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/FECKY-FARRIS.jpg"
                                             alt="event-image">
                                        <h4>Fecky Farris</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 23.00-00.00</h6>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/CASS.jpg" alt="event-image">
                                        <h4>CASS</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 04.00-The end</h6>-->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/UNA-ANDREA.jpg"
                                             alt="event-image">
                                        <h4>Una Andrea</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 01.00-02.30</h6>-->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/FIX.jpg" alt="event-image">
                                        <h4>FIX</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 02.30-04.00</h6>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="images/performers/mirko_popov.jpg"
                                             alt="event-image">
                                        <h4>Mirko Popov</h4>
                                        <h5>23.07.2016</h5>
                                        <!--<h6>Play time: 04.00-The end</h6>-->
                                    </div>
                                </div>
                                <!--<div class="col-sm-4">-->
                                <!--&lt;!&ndash;<div class="single-event">&ndash;&gt;-->
                                <!--&lt;!&ndash;<img class="img-responsive" src="images/performers/una%20andrea.jpg" alt="event-image">&ndash;&gt;-->
                                <!--&lt;!&ndash;<h4>Mike Shinoda</h4>&ndash;&gt;-->
                                <!--&lt;!&ndash;<h5>vocals, rhythm guitar</h5>&ndash;&gt;-->
                                <!--&lt;!&ndash;</div>&ndash;&gt;-->
                                <!--</div>-->
                                <!--<div class="col-sm-4">-->
                                <!--&lt;!&ndash;<div class="single-event">&ndash;&gt;-->
                                <!--&lt;!&ndash;<img class="img-responsive" src="images/performers/fix.jpg" alt="event-image">&ndash;&gt;-->
                                <!--&lt;!&ndash;<h4>Rob Bourdon</h4>&ndash;&gt;-->
                                <!--&lt;!&ndash;<h5>drums</h5>&ndash;&gt;-->
                                <!--&lt;!&ndash;</div>&ndash;&gt;-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                    <!--<div class="guitar">-->
                    <!--<img class="img-responsive" src="images/guitar.png" alt="guitar">-->
                    <!--</div>-->
                </div>
            </div>
</section><!--/#event-->

<section id="about">
    <div class="about-content">
        <h2>Mavrovo Open 2016</h2>
        <p>Since its beginning, Paragliding club - Condor conducted 3 successful projects that were
            financed by local and foreign donors, focusing on the promotion of paragliding as a form of extreme sports.
            In 2012, under the name “Gostivar paragliding cup 2012”, Condor organized the first international contest in
            precise landing.
            The contest was visited by 46 pilots-contestants from Macedonia, Albania, Kosovo, Turkey and Bulgaria. The
            event was widely
            recognized by all paragliders in Macedonia, as well as the locals. The contest took a traditional character
            and was held in
            the years to come, respectively 2013 and 2014. In June 2015, an USAID project allowed Condor to organize a
            big paragliding
            contest, named “Gostivar Open 2015”. This contest gave birth to the “Korito” flying track in Suva Gora,
            which was inaugurated
            by the Deputy US Ambassador, the Mayor of Gostivar and the president of Condor, M-r Mentor Saiti.<br/><br/>
            This year, the event will be held by the lake of Mavrovo. It will bring in pilots from across Europe and
            Asia as
            well as some of the most famous names in the acrobatic and precise paragliding. The official paragliding
            competition
            in ‘precise landing’ is organized in compliance with the strict regulations of the FAI (<a
                href="http://www.fai.org/" target="_blank"> Fédération Aéronautique Internationale</a>).<br/><br/>
            In addition to the paragliding contest, a cycling tour around lake Mavrovo and an open air party wilth live
            bands and DJs from the region will be taking place during the event. The cycling tour and competition will
            include all bicycling clubs from the Republic of Macedonia along with few from the neighborhood.
        </p>
    </div>
    <div class="about-content">
        <div>
            <h2>MAVROVO OPEN 2016 OFFICIAL AGENDA</h2><br/>
            <div>
                <h3><b>Friday 22.07.2016</b></h3>
                <div>
                    <p><b>18.00-23.00</b>&nbsp;&nbsp;Hotel Mavrovo, Arrival of international guests</p>
                </div>
                <h3><b>Saturday 23.07.2016</b></h3>
                <div>
                    <p><b>10.00-10.30</b>&nbsp;&nbsp;Ceremonial opening of Mavrovo Open 2016 at Mavrovo Stadium</p>
                    <p><b>11.00-15.00</b>&nbsp;&nbsp;Bicycling race around Lake Mavrovo. Start/Finish at the large
                        parking area in front of “Zare Lazarevski” Ski Center</p>
                    <p><b>11.00-16.00</b>&nbsp;&nbsp;Opening round of flights with paragliders. Take off – Main ski path
                        (single ski chairlift) | Landing target – Mavrovo Stadium. </p>
                    <p><b>16.00-17.00</b>&nbsp;&nbsp;Tandem flights for the interested parties. Taking off (with a
                        motorized paraglider) from the location called “Kamp” by the lakeside. Acrobatic flights with a
                        paraglider and motorized paraglider. Air stunts with a para-motor and a parachute. Tandem
                        flights for the interested parties. </p>
                    <p><b>17.00–18.00 </b>&nbsp;&nbsp;Closing of the cycling race. Announcement of winners of the
                        bicycling race and prize awards for the cyclists.</p>
                    <p><b>18.00-05.00</b>&nbsp;&nbsp;Open air party with live bands and DJ-s “Mavrovo Open Air Party” at
                        Mavrovo Stadium</p>
                </div>
                <h3><b>Sunday 24.07.2016</b></h3>
                <div>
                    <p><b>10.00-13.00</b>&nbsp;&nbsp;Second round of flights with paragliders. Start at main ski path
                        (single ski chairlift) | Landing – Mavrovo Stadium. Tandem flights for the interested parties.
                        Air stunts with para-motor and paragliders </p>
                    <p><b>13.00-15.00</b>&nbsp;&nbsp;Third round of flights with paragliders. Start at main ski path
                        (single ski chairlift) | landing at Mavrovo Stadium.
                        Tandem flights for the interested parties.
                    </p>
                    <p><b>15.00-16.00</b>&nbsp;&nbsp;Ceremonial closing of the contest and prize awards at Mavrovo
                        Stadium</p>
                </div>
            </div>
        </div>
    </div>
</section><!--/#about-->

<section id="twitter">
    <div id="twitter-feed" class="carousel slide" data-interval="false">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="text-center carousel-inner center-block">
                    <div class="item active">
                        <img src="images/twitter/Mentor Saiti.jpg" alt="">
                        <p>Mentor Saiti<br/>
                            Event Director </p>
                        <a href="https://www.facebook.com/Paragliding-Club-Condor-254174251319818/" target="_blank">Paragliding
                            Club
                            CONDOR</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/Lirim Hajredini.jpg" alt="">
                        <p>Lirim Hajredini <br/> Member </p>
                        <a href="http://alsat-m.tv/" target="_blank">
                            ALSAT M TV</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/Mendo Veljanovski.jpg" alt=""/>
                        <p>Mendo Veljanovski<br/>Member </p>
                        <a href="http://www.fai.org/" target="_blank">
                            International Paragliding Association
                            (Master Instructor at API/FAI)</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/agron_kurtishi.jpg" alt="">
                        <p>Agron Kurtishi <br/>Member</p>
                        <a href="http://www.leoron.com/" target="_blank">
                            LEORON Group</a>
                    </div>


                    <div class="item">
                        <img src="images/twitter/arian_jusufi.jpg" alt="">
                        <p>Arian Jusufi <br/>PR Manager </p>
                        <a href="http://www.leoron.com/" target="_blank">
                            LEORON Group</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/Imran Shabani.jpg" alt="">
                        <p>Imran Shabani<br/>Music Events Coordinator </p>
                        <a href="http://www.leoron.com/" target="_blank">
                            LEORON Group</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/atanas_boskov.jpg" alt="">
                        <p>Atanas Boshkov<br/>Sponsorship Events Manager</p>
                        <a href="http://www.leoron.com/" target="_blank">
                            LEORON Group</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/martin_sokolovski.jpg" alt="">
                        <p>Martin Sokolovski<br/>Sponsorship Events Manager </p>
                        <a href="http://www.leoron.com/" target="_blank">
                            LEORON Group</a>
                    </div>
                    <div class="item">
                        <img src="images/twitter/valdet_emini.jpg" alt="">
                        <p>Valdet Emini<br/>Sponsorship Events Manager </p>
                        <a href="https://www.facebook.com/Paragliding-Club-Condor-254174251319818/" target="_blank">
                            Paragliding Club Condor</a>
                    </div>
                </div>
                <a class="twitter-control-left" href="#twitter-feed" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="twitter-control-right" href="#twitter-feed" data-slide="next"><i
                        class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</section><!--/#twitter-feed-->
<section id="gallery">
    <div id="carousel-example-generic" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
            <li data-target="#carousel-example-generic" data-slide-to="5"></li>
            <li data-target="#carousel-example-generic" data-slide-to="6"></li>
            <li data-target="#carousel-example-generic" data-slide-to="7"></li>
            <li data-target="#carousel-example-generic" data-slide-to="8"></li>
            <li data-target="#carousel-example-generic" data-slide-to="9"></li>
            <li data-target="#carousel-example-generic" data-slide-to="10"></li>
            <li data-target="#carousel-example-generic" data-slide-to="11"></li>
            <li data-target="#carousel-example-generic" data-slide-to="12"></li>
            <li data-target="#carousel-example-generic" data-slide-to="13"></li>
            <li data-target="#carousel-example-generic" data-slide-to="14"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/slider/mavrovo1.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Concerts during the Mavrovo Open 2015</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo2.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo3.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo4.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo5.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo6.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo7.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo8.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo9.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo10.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo11.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo12.JPG" alt="...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <img src="images/slider/mavrovo13.JPG" alt=...">
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <iframe width="1600" height="900" src="https://www.youtube.com/embed/TJGgKrW8vIg" frameborder="0"
                        allowfullscreen></iframe>
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
            <div class="item">
                <iframe width="1600" height="900" src="https://www.youtube.com/embed/vhCC13YswQs" frameborder="0"
                        allowfullscreen></iframe>
                <!--<div class="carousel-caption">-->
                <!--<h3>Caption Text</h3>-->
                <!--</div>-->
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
    <br/>

</section>
<section id="sponsor">
    <div id="sponsor-carousel" class="carousel slide" data-interval="false">
        <div class="container">
            <div class="row" align="center">
<!--                <div class="col-sm-9">-->
                    <!--                    <p class="sponsor-heading">Organizer, Supporter &amp; Exclusive Media Partner</p>-->
                    <!--<a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i-->
                    <!--class="fa fa-angle-left"></i></a>-->
                    <!--<a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i-->
                    <!--class="fa fa-angle-right"></i></a>-->
                    <div class="carousel-inner">
                        <div class="item active">
                            <ul>
                                <li>
                                    <div>
                                        <p class="sponsor-heading">Organized by</p>
                                        <a href="https://www.facebook.com/Paragliding-Club-Condor-254174251319818/"
                                           target="_blank"><img class="img-responsive sponsors-organizers"
                                                                src="images/sponsor/condor.png"
                                                                alt="Paragliding Club Condor"></a>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p class="sponsor-heading">Supported by</p>
                                        <a href="http://www.leoron.com/" target="_blank"><img
                                                class="img-responsive sponsors-organizers"
                                                src="images/sponsor/leoron.png"
                                                alt="LEORON Professional Development Institute"></a>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p class="sponsor-heading">
                                            Exclusive Media Partner
                                        </p>
                                        <a href="http://alsat-m.tv/" target="_blank"><img
                                                class="img-responsive sponsors-organizers"
                                                src="images/sponsor/alsat.png"
                                                alt="Alsat TV"></a>
                                    </div>
                                </li>
                            </ul>
                            <hr ><br/>
                            <div>
                                <p class="sponsor-heading">Media Partners</p><br/>
                                <ul>
                                    <li>
                                        <a href="http://www.almakos.com/" target="_blank"><img
                                                class="img-responsive media-coverage"
                                                src="images/sponsor/almakos.png"
                                                alt="Almakos"></a>
                                    </li>
                                    <li>
                                        <a href="http://lajmpress.com/" target="_blank"><img
                                                class="img-responsive media-coverage"
                                                src="images/sponsor/lajm.png"
                                                alt="LAJM"></a>
                                    </li>
                                    <li>
                                        <a href="http://www.koha.mk/" target="_blank"><img
                                                class="img-responsive media-coverage"
                                                src="images/sponsor/Koha.png"
                                                alt="Koha"></a>
                                    </li>
                                </ul>
                                <hr/><br/>
                            </div>
                            <div>
                                <p class="sponsor-heading">Supporters</p><br/>
                                <ul>
                                    <li><a href="http://npmavrovo.org.mk/en/" target="_blank"><img
                                                class="img-responsive"
                                                src="images/sponsor/npmavrovo.png"
                                                alt="National Park Mavrovo"></a>
                                    </li>
                                    <li><a href="http://www.mavrovoirostuse.gov.mk/" target="_blank"><img
                                                class="img-responsive" src="images/sponsor/mavrovorostuse.png"
                                                alt="Municipality of Mavrovo-Rostuse"></a></li>
                                </ul>
                                <hr/><br/>
                                <div>
                                    <p class="sponsor-heading">Sports Ambassadors</p><br/>
                                    <ul>
                                        <li>
                                            <a href="http://www.ladna.com.mk/ladna-juicy" target="_blank">
                                                <img class="img-responsive media-coverage"
                                                     src="images/sponsor/juicy.png" alt="Juicy Ladna"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://web.facebook.com/studioinbox/" target="_blank">
                                                <img class="img-responsive media-coverage"
                                                     src="images/sponsor/inbox-logo.jpg" alt="Studio Inbox"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://www.savana.mk/" target="_blank">
                                                <img class="img-responsive savana-tours-ambassador"
                                                     src="images/sponsor/savanna.jpg" alt="Savana Tourist Agency"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--<div class="item">-->
                        <!--<ul>-->
                        <!--<li></a></li>-->
                        <!--<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png"-->
                        <!--alt=""></a></li>-->
                        <!--<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png"-->
                        <!--alt=""></a></li>-->
                        <!--<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png"-->
                        <!--alt=""></a></li>-->
                        <!--<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png"-->
                        <!--alt=""></a></li>-->
                        <!--<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png"-->
                        <!--alt=""></a></li>-->
                        <!--</ul>-->
                        <!--</div>-->
                    </div>
                </div>
                <!--                <div style="padding-left: 10px;">-->
                <!--                    <h2>Media Partners</h2>-->
                <!--                    <ul>-->
                <!--                        <li>-->
                <!--                            <a href="http://alsat-m.tv/" target="_blank"><img class="img-responsive"-->
                <!--                                                                              src="images/sponsor/Alsat.png"-->
                <!--                                                                              alt="Alsat TV" \></a>-->
                <!--                        </li>-->
                <!--                        <li>-->
                <!--                            <a href="http://www.almakos.com/" target="_blank"><img class="img-responsive" style="width: 25%; padding-left: 10px;"-->
                <!--                                                                                   src="images/sponsor/Almakos_Logo_Fin.jpg"-->
                <!--                                                                                   alt="Almakos" \></a>-->
                <!--                        </li>-->
                <!--                    </ul>-->
                <!--                    <ul>-->
                <!--                       <li>-->
                <!--                           <a href="http://lajmpress.com/" target="_blank"><img class="img-responsive" style="width: 25%; padding-left: 10px;"-->
                <!--                                                                                src="images/sponsor/LOGO-LAJM-NEW-2016-vector.jpg"-->
                <!--                                                                                alt="LAJM" \></a>-->
                <!--                       </li>-->
                <!--                        <li>-->
                <!--                            <a href="http://www.koha.mk/" target="_blank"><img class="img-responsive"-->
                <!--                                                                               src="images/sponsor/Koha.png"-->
                <!--                                                                               alt="Koha" style="width: 25%; padding-left: 10px;"\></a>-->
                <!--                        </li>-->
                <!--                    </ul>-->
                <!--                </div>-->
            </div>
        </div>

    </div>
</section><!--/#sponsor-->

<section id="contact">
    <div id="map">
        <div id="gmap-wrap">
            <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7090.023111369012!2d20.73035126196782!3d41.65601188494937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x135141f7ab832203%3A0x932360f3c90dd8e8!2sMavrovo!5e0!3m2!1sen!2smk!4v1465979132722"-->
            <!--frameborder="0"  allowfullscreen></iframe>-->
            <iframe src="//ridewithgps.com/routes/1350422/embed" style="border:0; width:100%;
	min-height:600px;" frameborder="0"></iframe>
        </div>
    </div><!--/#map-->
    <section id="contact-sect">
        <div class="container" style="height: 500px; align-content: center">
            <h2 style="color: #000000" class="text-center">If you are a pilot and you would like to participate,<br/>
                please follow the link below</h2><br/><br/><br/><br/>
            <center><a href="https://docs.google.com/forms/d/1WGLVRgjC1jLZwQBBadcuzFRWVK1pc6H_CHq8ELATMFE/viewform "
                       class="btn btn-xs btn-register" role="button" target="_blank">Register</a></center>
        </div>
        <div class="contact-section">
            <!--<div class="ear-piece">-->
            <!--<img class="img-responsive" src="images/ear-piece.png" alt="">-->
            <!--</div>-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-4">
                        <div class="contact-text">
                            <h4 class="text-color">Contact</h4>
                            <address>
                                E-mail: info@mavrovoopen.mk<br>
                                Phone: +389 78 24 23 20<br>
                                Phone 2: +389 71 31 71 03
                            </address>
                        </div>
                        <div class="contact-address">
                            <h4 class="text-color">Contact</h4>
                            <address>
                                International Business Park<br>
                                Str. 20, No 82, Brazda, Cucer-Sandevo<br>
                                1000 Skopje<br>
                                Macedonia
                            </address>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div id="contact-section">
                            <h3 class="text-color">Send a message</h3>
                            <div class="status alert alert-success" style="display: none"></div>
                            <form id="main-contact-form" class="contact-form" name="contact-form" method="post">

                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" required="required"
                                           placeholder="Name" id="name">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" required="required"
                                           placeholder="Email ID" id="email">
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="message" required="required" class="form-control"
                                              rows="4" placeholder="Enter your message"></textarea>
                                </div>
                                <div class="form-group" id="send_success_contact" style="display: none">
                                    <p>The Email has been successfull sent!</p>
                                </div>
                                <div class="form-group" id="send_error_contact" style="display: none">
                                    <p style="color: red;">Email while trying to send the email!</p>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-right" id="submit">Send</button>
                                </div>
                            </form>
                            <!--action="sendemail.php"
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#contact-->
    <footer id="footer">
        <div class="container">
            <div class="text-center">
                &copy;<a target="_blank" href="http://www.mavrovoopen.mk/"> Mavrovo Open</a> 2016. All Rights
                Reserved.<br/>
            </div>
            <div class="text-center">
                Follow us:
                <div class="social-icons ">
                    <a href="https://www.facebook.com/mavrovoopen/" target="_blank"><i
                            class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i> </a>
                    <!--<a href="#"><i class="fa fa-google-plus"></i></a>-->
                    <a href="https://www.youtube.com/channel/UCd_e5OCfA8QhqmheNkH5UIA" target="_blank"><i
                            class="fa fa-youtube"></i></a>
                </div>
            </div>
            <br/>
            <div class="text-center">
                Design &amp; maintenance by <a href="http://www.popleads.com/" target="_blank">Popleads</a>
            </div>
        </div>
    </footer>
    <!--/#footer-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script>
        $('#submit').click(function () {

            // $(this).attr('disabled', true);

            var name = $('#name').val();
            var email = $('#email').val();
            var message = $('#message').val();

            var form_data = {
                ajax: '1',
                name: name,
                email: email,
                text: message
            }

            $.ajax({
                url: "http://www.mavrovoopen.mk/sendmail.php",
                type: 'POST',
                async: false,
                data: form_data,
                success: function (response) {

                    var obj = jQuery.parseJSON(response);

                    if (obj.status == 'success') {
                        $("#send_success_contact").show();
                    }
                    else {
                        $("#send_error_contact").show();
                    }

                }
            });

            return false;

        });

    </script>
</body>
</html>